from dataclasses import field, dataclass
from typing import Union


@dataclass
class BedpeRecord(object):
    chr1: str
    begin1: int
    end1: int
    chr2: str
    begin2: int
    end2: int
    value: Union[int, float]

    @property
    def anchor_1_length(self):
        return self.end1 - self.begin1

    @property
    def anchor_2_length(self):
        return self.end2 - self.begin2

    @property
    def total_length(self):
        return self.b - self.a

    @property
    def a(self):
        return (self.begin1 + self.end1) // 2

    @property
    def b(self):
        return (self.begin2 + self.end2) // 2

    def __str__(self):
        return f'{self.chr1}\t{self.begin1}\t{self.end1}\t{self.chr2}\t{self.begin2}\t{self.end2}\t{self.value}'
