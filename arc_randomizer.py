#!/usr/bin/env python3

import argparse
import csv
from typing import List

import numpy as np

from datatypes import BedpeRecord


def read_bedpe(filename: str) -> List[BedpeRecord]:
    """Read BEDPE file with interactions."""
    data = []
    with open(filename) as f:
        reader = csv.reader(f, delimiter='\t')
        for i in reader:
            c1, b1, e1, c2, b2, e2, v = i
            b1, e1, b2, e2, v = [int(i) for i in (b1, e1, b2, e2, v)]
            interaction = BedpeRecord(c1, b1, e1, c2, b2, e2, v)
            data.append(interaction)
    return data


def randomize_coverage(interactions: List[BedpeRecord], begin: int, end: int):
    assert begin < end
    for i in interactions:
        new_a1 = np.random.randint(begin, end - i.total_length)
        new_b1 = new_a1 + i.anchor_1_length
        new_b2 = new_a1 + i.total_length
        new_a2 = new_b2 - i.anchor_2_length
        i.begin1 = new_a1
        i.end1 = new_b1
        i.begin2 = new_a2
        i.end2 = new_b2


def save_new_file(interactions: List[BedpeRecord], outfile: str) -> None:
    w = ''
    for i in interactions:
        w += str(i) + '\n'
    w = w[:-1]
    with open(outfile, 'w') as f:
        f.write(w)
    print(f"File {outfile} saved.")


def main():
    parser = argparse.ArgumentParser(description='Shuffle interactions in BEDPE file. Does not change interactions lengths, and values.')
    parser.add_argument('infile', help="path to bedpe file with interactions. Must contain only one chromosome.")
    parser.add_argument('begin', type=int, help="begin of region (int)")
    parser.add_argument('end', type=int, help="end of region (int)")
    parser.add_argument('outfile', help="path to randomized bedpe file")
    args = parser.parse_args()

    interactions = read_bedpe(args.infile)
    randomize_coverage(interactions, args.begin, args.end)
    save_new_file(interactions, args.outfile)


if __name__ == '__main__':
    main()
